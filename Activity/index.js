// console.log("Hello World")


let users = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista'];

console.log("Original Array: ")
console.log(users);

// #1
function addItem(item) {
	users[users.length++] = item;
}

addItem("John Cena");
console.log(users)

//#2
function getItemByIndex(index) {
	return users[index]
}

let itemFound = getItemByIndex(2);
console.log(itemFound);

//#3
function deleteItem(){
	let deleted = users[users.length-1]
	users.length--;
	return deleted;
}

let deletedItem = deleteItem();
console.log(deletedItem);
console.log(users);

// #4
function updateItemByIndex(update, index) {
	users[index] = update;
}

updateItemByIndex("Triple H", 3);
console.log(users);

// #5

function deleteAll() {
	users = [];
}
deleteAll();
console.log(users);

//  #6

function checkEmpty(array) {
	if (users.length > 0) { 
		return false
	} else {
		return true;
	}
}

let isUserEmpty = checkEmpty();
console.log(isUserEmpty);
